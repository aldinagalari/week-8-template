from flask import Flask, request, abort
import telebot

app = Flask(__name__)
app.config.from_object('{}.config'.format(__name__))

bot = telebot.TeleBot(app.config['TELEGRAM_BOT_TOKEN'], threaded=False)

from . import main  # noqa

# Why do this? See https://core.telegram.org/bots/api#setwebhook
webhook_url_base = app.config['WEBHOOK_HOST']

# Configure application logging
app.logger.setLevel(app.config['LOG_LEVEL'])


@app.route('/')
def index():
    return 'Bot is Running'


@app.route('/bot', methods=['POST'])
def webhook():
    if request.headers.get('content-type') == 'application/json':
        bot.process_new_updates([telebot.types.Update.de_json
                                 (request.get_data().decode("utf-8"))])
        return "!", 200
    else:
        return abort(403)


if app.config['APP_ENV'] != 'development':  # pragma: no cover
    bot.set_webhook(url='{}'.format(webhook_url_base))
