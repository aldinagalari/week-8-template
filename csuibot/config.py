from os import environ

APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
KEY = '378186858:AAFJK_bQeed1t-805Y_EFRN-xs0pSEYmD4Q'

TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN', KEY)
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://nameless-basin-17173.herokuapp.com/bot')
